# -*- coding: utf-8 -*-
from datetime import date

import numpy as np
import pytest

from csnake import cconstructs as cc
from csnake._version import __version__
from csnake.codewriter import CodeWriter
from csnake.codewriter import DefStackEmptyError


class TestCodeWriter:
    def test_init_exceptions(self):
        with pytest.raises(TypeError):
            CodeWriter(lf=2, indent=4)

        with pytest.raises(TypeError):
            CodeWriter("\n", indent=[])

    def test_init_ok(self):
        CodeWriter()
        CodeWriter(lf="#")
        CodeWriter(indent="#")
        CodeWriter(indent=4)

    def test_add_autogen_comment(self):
        cw1 = CodeWriter()
        cw1.add_autogen_comment()
        assert (
            str(cw1)
            == f"""\
/*
* This file was automatically generated using csnake v{__version__}.
*
* This file should not be edited directly, any changes will be
* overwritten next time the script is run.
*
* Source code for csnake is available at:
* https://gitlab.com/andrejr/csnake
*
* csnake is also available on PyPI, at :
* https://pypi.org/project/csnake
*/"""
        )

        cw2 = CodeWriter()
        cw2.add_autogen_comment("test_codewriter.py")
        print(cw2)
        assert (
            str(cw2)
            == f"""\
/*
* This file was automatically generated using csnake v{__version__}.
*
* This file should not be edited directly, any changes will be
* overwritten next time the script is run.
* Make any changes to the file 'test_codewriter.py', not this file.
*
* Source code for csnake is available at:
* https://gitlab.com/andrejr/csnake
*
* csnake is also available on PyPI, at :
* https://pypi.org/project/csnake
*/"""
        )

    def test_add_license_comment(self):
        license_text = "\n".join(("license", "text", "lines"))
        intro_text = "\n".join(("intro", "text", "lines"))
        authors = [
            {"name": "Author Surname"},
            {"name": "Other Surname", "email": "test@email"},
        ]
        year = date.today().year

        cw1 = CodeWriter()
        cw1.add_license_comment(license_text)
        assert (
            str(cw1)
            == """\
/*
* license
* text
* lines
*/"""
        )

        cw2 = CodeWriter()
        cw2.add_license_comment(license_text, intro=intro_text)
        assert (
            str(cw2)
            == """\
/*
* intro
* text
* lines
*
* license
* text
* lines
*/"""
        )

        cw3 = CodeWriter()
        cw3.add_license_comment(
            license_text, authors=authors, intro=intro_text
        )
        assert (
            str(cw3)
            == f"""\
/*
* intro
* text
* lines
*
* Copyright © {year} Author Surname
* Copyright © {year} Other Surname <test@email>
*
* license
* text
* lines
*/"""
        )

        cw4 = CodeWriter()
        cw4.add_license_comment(
            license_text, authors=authors, intro=intro_text, year=3000
        )
        assert (
            str(cw4)
            == """\
/*
* intro
* text
* lines
*
* Copyright © 3000 Author Surname
* Copyright © 3000 Other Surname <test@email>
*
* license
* text
* lines
*/"""
        )

    def test_add_define(self):
        cw1 = CodeWriter()
        cw1.add_define("DEF1")
        cw1.add_define("DEF2", 2)
        cw1.add_define("DEF3", 2, "comment")

        assert (
            str(cw1)
            == """\
#define DEF1
#define DEF2 2
#define DEF3 2 /* comment */"""
        )

        cw2 = CodeWriter()
        cw2.add_define("DEF1", 2)
        cw2.add_define("DEF2", True)
        cw2.add_define("DEF3", "tstr")

        assert (
            str(cw2)
            == '''\
#define DEF1 2
#define DEF2 true
#define DEF3 "tstr"'''
        )

    def test_if_def_ok(self):
        cw1 = CodeWriter()
        cw1.start_if_def("TESTIF")
        cw1.end_if_def()

        assert (
            str(cw1)
            == """\
#ifdef TESTIF
#endif /* TESTIF */"""
        )

        cw2 = CodeWriter()
        cw2.start_if_def("TESTIF", invert=True)
        cw2.end_if_def()

        assert (
            str(cw2)
            == """\
#ifndef TESTIF
#endif /* TESTIF */"""
        )

    def test_if_def_nested_ok(self):
        cw1 = CodeWriter()
        cw1.start_if_def("TESTIF")
        cw1.start_if_def("NESTIF")
        cw1.end_if_def()
        cw1.end_if_def()

        assert (
            str(cw1)
            == """\
#ifdef TESTIF
#ifdef NESTIF
#endif /* NESTIF */
#endif /* TESTIF */"""
        )

    def test_if_def_nested_error(self):
        cw1 = CodeWriter()
        cw1.start_if_def("TESTIF")
        cw1.start_if_def("NESTIF")
        cw1.end_if_def()
        cw1.end_if_def()

        with pytest.raises(DefStackEmptyError):
            cw1.end_if_def()

    def test_cpp_entry_exit(self):
        cw = CodeWriter()
        cw.cpp_entry()
        cw.add_line("some_code;")
        cw.cpp_exit()

        assert (
            str(cw)
            == """\
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
some_code;
#ifdef __cplusplus
}
#endif /* __cplusplus */"""
        )

    def test_switch(self):
        cw = CodeWriter()
        var = cc.Variable("somevar", "int")
        case_var = cc.Variable("case_var", "int")
        cw.start_switch(var)

        cw.add_switch_case(2)
        cw.add_switch_break()
        cw.add_switch_case(case_var)
        cw.add_switch_return(5)
        cw.add_switch_default()
        cw.add_switch_return(8)
        cw.end_switch()

        assert (
            str(cw)
            == """\
switch (somevar)
{
    case 2:
        break;
    case case_var:
        return 5;
    default:
        return 8;
} /* ~switch (somevar) */"""
        )

    def test_include(self):
        cw1 = CodeWriter()
        cw1.include("someinclude.h")
        assert str(cw1) == '#include "someinclude.h"'

        cw2 = CodeWriter()
        cw2.include('"someinclude.h"')
        assert str(cw2) == '#include "someinclude.h"'

        cw2 = CodeWriter()
        cw2.include("<someinclude.h>")
        assert str(cw2) == "#include <someinclude.h>"

    def test_add_enum(self):
        cw1 = CodeWriter()

        name = "somename"
        pfx = "pfx"
        typedef = False
        newenum = cc.Enum(name, prefix=pfx, typedef=typedef)

        cval1 = cc.Variable("varname", "int")

        newenum.add_value("val1", cc.Dereference(1000))
        newenum.add_value("val2", cval1)
        newenum.add_value("val3", cc.AddressOf(cval1), "some comment")

        assert newenum.generate_declaration

        cw1.add_enum(newenum)

        assert str(cw1) == str(newenum)

    def test_add_variable_declaration(self):
        cw = CodeWriter()
        var = cc.Variable(
            "test", primitive="int", value=np.arange(24).reshape((2, 3, 4))
        )

        cw.add_variable_declaration(var)

        assert str(var.generate_declaration() + ";") == str(cw)

    def test_add_variable_initialization(self):
        cw = CodeWriter()
        var = cc.Variable(
            "test", primitive="int", value=np.arange(24).reshape((2, 3, 4))
        )

        cw.add_variable_initialization(var)

        assert str(var.generate_initialization()) == str(cw)

    def test_add_struct(self):
        cw = CodeWriter()

        strct = cc.Struct("strname", typedef=False)
        var1 = cc.Variable("var1", "int")
        var2 = cc.Variable("var2", "int", value=range(10))
        strct.add_variable(var1)
        strct.add_variable(var2)
        strct.add_variable(("var3", "int"))
        strct.add_variable({"name": "var4", "primitive": "int"})

        cw.add_struct(strct)
        assert str(strct.generate_declaration()) == str(cw)

    def test_add_function_prototype(self):
        cw = CodeWriter()

        arg1 = cc.Variable("arg1", "int")
        arg2 = cc.Variable("arg2", "int", value=range(10))
        arg3 = ("arg3", "int")
        arg4 = {"name": "arg4", "primitive": "int"}
        fun = cc.Function(
            "testfunct", "void", arguments=(arg1, arg2, arg3, arg4)
        )
        fun.add_code(("code;", "more_code;"))

        cw.add_function_prototype(fun)

        assert fun.generate_prototype() + ";" == str(cw)

    def test_add_function_definition(self):
        cw = CodeWriter()

        arg1 = cc.Variable("arg1", "int")
        arg2 = cc.Variable("arg2", "int", value=range(10))
        arg3 = ("arg3", "int")
        arg4 = {"name": "arg4", "primitive": "int"}
        fun = cc.Function(
            "testfunct", "void", arguments=(arg1, arg2, arg3, arg4)
        )
        fun.add_code(("code;", "more_code;"))

        cw.add_function_definition(fun)

        assert str(fun.generate_definition()) == str(cw)

    def test_add_function_call(self):
        cw = CodeWriter()

        arg1 = cc.Variable("arg1", "int")
        arg2 = cc.Variable("arg2", "int", value=range(10))
        arg3 = ("arg3", "int")
        arg4 = {"name": "arg4", "primitive": "int"}
        fun = cc.Function(
            "testfunct", "void", arguments=(arg1, arg2, arg3, arg4)
        )

        argument_values = (2, 3, 4, 5)

        cw.add_function_call(fun, argument_values)

        assert fun.generate_call(argument_values) + ";" == str(cw)

    def test_write_to_file(self, tmpdir):
        cw = CodeWriter()
        var = cc.Variable(
            "test", primitive="int", value=np.arange(24).reshape((2, 3, 4))
        )
        cw.add_variable_initialization(var)

        filename = "test.c"
        path = tmpdir.join(filename)
        cw.write_to_file(path)

        assert path.read() == cw.code
