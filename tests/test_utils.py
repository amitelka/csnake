# -*- coding: utf-8 -*-
import pytest

from csnake import utils


class TestAssureStr:
    def test_notstr(self):
        with pytest.raises(TypeError):
            utils.assure_str(2)

        with pytest.raises(TypeError):
            utils.assure_str(None)

    def test_str(self):
        somestr = "string"
        assert utils.assure_str(somestr) == somestr


class TestAssureStrOrNone:
    def test_notstr(self):
        with pytest.raises(TypeError):
            utils.assure_str_or_none(2)

    def test_none(self):
        assert utils.assure_str_or_none(None) is None

    def test_str(self):
        somestr = "string"
        assert utils.assure_str_or_none(somestr) == somestr


class TestSeqGet:
    def test_seq_get(self):
        seq = (1, 2)
        defaults = ("a", "b", "c")
        assert utils.seq_get(seq, defaults, 1) == seq[1]

    def test_default_get(self):
        seq = (1, 2)
        defaults = ("a", "b", "c")
        assert utils.seq_get(seq, defaults, 2) == defaults[2]

    def test_neither_get(self):
        seq = (1, 2)
        defaults = ("a", "b", "c")
        with pytest.raises(IndexError):
            utils.seq_get(seq, defaults, 4)

    def test_negative_index_in_range(self):
        seq = (1, 2, 3, 4, 5)
        defaults = ("a", "b", "c")
        neg_ind = -1
        assert (
            utils.seq_get(seq, defaults, neg_ind)
            == seq[len(defaults) + neg_ind]
        )

    def test_negative_index_out_of_range(self):
        seq = (1, 2, 3, 4, 5)
        defaults = ("a", "b", "c")
        neg_ind = -4
        with pytest.raises(IndexError):
            utils.seq_get(seq, defaults, neg_ind)


class TestSublistsFromIterable:
    def test_not_iterable(self):
        with pytest.raises(TypeError):
            sfi = utils.sublists_from_iterable(3, 2)
            next(iter(sfi))

    def test_length_ok_inc(self):
        inp = (1 * a for a in range(4))
        sfi = utils.sublists_from_iterable(inp, 2, True)

        assert list(sfi) == [[0, 1], [2, 3]]

    def test_length_ok_ninc(self):
        inp = (1 * a for a in range(4))
        sfi = utils.sublists_from_iterable(inp, 2, False)

        assert list(sfi) == [[0, 1], [2, 3]]

    def test_length_notok_inc(self):
        inp = (1 * a for a in range(4))
        sfi = utils.sublists_from_iterable(inp, 3, True)

        assert list(sfi) == [[0, 1, 2], [3]]

    def test_length_notok_ninc(self):
        inp = (1 * a for a in range(4))
        sfi = utils.sublists_from_iterable(inp, 3, False)

        with pytest.raises(ValueError):
            list(sfi)


class TestRgb888ToRgb565:
    def test_ok_ints(self):
        assert utils.rgb888_to_rgb565((255, 128, 14)) == 0xFBE1

    def test_ok_bytes(self):
        assert utils.rgb888_to_rgb565(bytes([255, 128, 14])) == 0xFBE1

    def test_notseq(self):
        with pytest.raises(TypeError):
            utils.rgb888_to_rgb565(2)

    def test_wrongseq(self):
        with pytest.raises(TypeError):
            utils.rgb888_to_rgb565("abc")


class TestBytesToInt:
    def test_ints_ok(self):
        assert utils.bytes_to_int((0xAB, 0xCD, 0xEF)) == 0xABCDEF

    def test_bytes_ok(self):
        assert utils.bytes_to_int(bytes([0xAB, 0xCD, 0xEF])) == 0xABCDEF

    def test_gen_ok(self):
        assert (
            utils.bytes_to_int(1 * x for x in [0xAB, 0xCD, 0xEF]) == 0xABCDEF
        )

    def test_not_iterable(self):
        with pytest.raises(TypeError):
            utils.bytes_to_int(2)

    def test_wrong_iterable(self):
        with pytest.raises(TypeError):
            utils.bytes_to_int("abcd")
